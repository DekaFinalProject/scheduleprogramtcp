package main

import (
	"flag"
	"fmt"
	"log"
	"runtime"
	"sync"
)

var port = flag.Uint("port", 9654, "The listening port 0-65535")
var numthreads = flag.Uint("j", 128, "sets max thread count in go. Default is 128")

func main() {

	flag.Parse()
	if !flag.Parsed() {
		flag.PrintDefaults()
	}

	if 65535 < *port {
		log.Fatalf("Invalid port specified")
	}

	if *numthreads == 0 {
		runtime.GOMAXPROCS(128)
	} else {
		runtime.GOMAXPROCS(int(*numthreads))
	}

	var wg sync.WaitGroup
	wg.Add(1)
	var listener = NewListener(fmt.Sprintf(":%d", *port), &wg)
	go listener.Listen()

	listener.WaitForStop()
}
