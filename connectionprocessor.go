package main

import (
	"bufio"
	"container/list"
	"encoding/json"
	"log"
	"net"
	"sync"

	"gitlab.com/DekaFinalProject/dekaants"
	"gitlab.com/DekaFinalProject/scheduleantsolver"
	"gitlab.com/DekaFinalProject/schedulelib/schedule"
)

/*
JSONMessage messages to receive from clients
Types:
	- problem -- sets new problem
	- config -- sets solver configs
	- start -- starts solving
	- status -- gets status of solving
*/
type JSONMessage struct {
	Type string
	Data json.RawMessage
}

type connectionProcessor struct {
	conn  net.Conn
	chain *list.List

	problem schedule.Problem
	config  SolverOptions
	answer  scheduleantsolver.ScheduleAnswer

	solving      bool // Currently solving
	solvingMutex sync.Mutex
	solvingCond  *sync.Cond // Allows to block some operations until solving not ended

	answerReady  bool // answer can be fetched
	failed       bool
	failMsg      string
	statusLayers []*StatusLayer
	solvers      []dekaants.Solver
}

func newConnectionProcessor(conn net.Conn) *connectionProcessor {
	var out connectionProcessor

	out.conn = conn
	out.chain = list.New()

	out.solving = false
	out.answerReady = false
	out.failed = false
	out.failMsg = ""
	out.solvingCond = sync.NewCond(&out.solvingMutex)
	out.solvers = make([]dekaants.Solver, 0)

	// Add processor elements
	out.chain.PushBack(newGetStatusChainElement())
	out.chain.PushBack(newSetProblemChainElement())
	out.chain.PushBack(newSetSolverChainElement())
	out.chain.PushBack(newStartSolvingChainElement())

	// Set processor context
	for it := out.chain.Front(); it != nil; it = it.Next() {
		var value messageProcessChainElement = it.Value.(messageProcessChainElement)
		value.SetProcessor(&out)
	}

	return &out
}

func (p *connectionProcessor) Process(wg *sync.WaitGroup) {
	defer wg.Done()

	rw := bufio.NewReadWriter(bufio.NewReader(p.conn), bufio.NewWriter(p.conn))

	var err error = nil
	readed, err := rw.ReadString('\000')

	for err == nil {
		// Remove trailing NULL
		readed = readed[:len(readed)-1]

		var msg JSONMessage
		err = json.Unmarshal([]byte(readed), &msg)
		if err != nil {
			break
		}

		for it := p.chain.Front(); it != nil; it = it.Next() {
			var value messageProcessChainElement = it.Value.(messageProcessChainElement)

			var captured = false
			captured, err = value.Process(msg)

			if captured {
				break
			}
		}

		if err != nil {
			break
		}

		// Fetch new message
		readed, err = rw.ReadString('\000')
	}

	for _, s := range p.solvers {
		s.Stop()
	}

	log.Printf("Processor(%s) has finished work: %v", p.conn.RemoteAddr().String(), err)
}
