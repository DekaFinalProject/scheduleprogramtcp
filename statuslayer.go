package main

import "gitlab.com/DekaFinalProject/dekaants"

type StatusLayer struct {
	runNumber uint64
	iteration uint64
	minMetric float64
}

func newStatusLayer(runNumber uint64) dekaants.VerboseLayer {
	return &StatusLayer{
		runNumber: runNumber,
		iteration: 0,
		minMetric: 0,
	}
}

func (l *StatusLayer) OnIterationEnter(i uint64) {
	l.iteration = i
}

func (l *StatusLayer) OnIterationSolutionReady(ans dekaants.Answer) {}
func (l *StatusLayer) OnBestAnswerChanged(ans dekaants.Answer) {
	l.minMetric = ans.Metric()
}
func (l *StatusLayer) IterationMetrics(min, avg, max float64) {}
