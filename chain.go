package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"sync"

	"gitlab.com/DekaFinalProject/dekaants"
	"gitlab.com/DekaFinalProject/scheduleantsolver"
	"gitlab.com/DekaFinalProject/schedulelib/schedule"
	"gitlab.com/DekaFinalProject/schedulelib/schedule/simple"
)

type processorContextElement struct {
	processor *connectionProcessor
}

func (el *processorContextElement) SetProcessor(processor *connectionProcessor) {
	el.processor = processor
}

type messageProcessChainElement interface {
	Process(msg JSONMessage) (bool, error) // true value - if msg captured, error in case of errors(bool value in that case always true)
	SetProcessor(processor *connectionProcessor)
}

// Make problem
type setProblemChainElement struct {
	processorContextElement
}

func newSetProblemChainElement() messageProcessChainElement {
	return &setProblemChainElement{}
}

func (e *setProblemChainElement) Process(msg JSONMessage) (bool, error) {
	if msg.Type != "problem" {
		return false, nil
	}

	if e.processorContextElement.processor == nil {
		return true, errors.New("chain element initialization was invalid: No processor specified")
	}

	// Wait for solving completed
	for e.processorContextElement.processor.solving {
		e.processorContextElement.processor.solvingCond.Wait()
	}

	var loader = NewJSONProblemLoader(msg.Data, simple.NewFactory())
	problem, err := loader.Load()
	if err != nil {
		return true, err
	}

	e.processorContextElement.processor.problem = problem

	log.Printf(
		"Processor(%s) received problem: Task count: %d; Resource count: %d",
		e.processor.conn.RemoteAddr().String(),
		len(problem.Graph().TasksIDs()),
		problem.Resources().Len(),
	)

	return true, nil
}

// Solver config
type setSolverChainElement struct {
	processorContextElement
}

func newSetSolverChainElement() messageProcessChainElement {
	return &setSolverChainElement{}
}

func (el *setSolverChainElement) Process(msg JSONMessage) (bool, error) {
	var captured = false
	if msg.Type != "config" {
		return captured, nil
	}

	captured = true

	if el.processorContextElement.processor == nil {
		return true, errors.New("chain element initialization was invalid: No processor specified")
	}

	// Wait for solving completed
	for el.processorContextElement.processor.solving {
		el.processorContextElement.processor.solvingCond.Wait()
	}

	var config SolverOptions
	err := json.Unmarshal(msg.Data, &config)
	if err != nil {
		return captured, err
	}

	log.Printf("Processor(%s) received config: \n%s", el.processor.conn.RemoteAddr().String(), msg.Data)

	el.processorContextElement.processor.config = config
	return captured, nil
}

// Status get
type getStatusChainElement struct {
	processorContextElement
}

func newGetStatusChainElement() messageProcessChainElement {
	return &getStatusChainElement{}
}

func (el *getStatusChainElement) Process(msg JSONMessage) (bool, error) {
	var captured = false
	if msg.Type != "status" {
		return captured, nil
	}

	captured = true

	var proc = el.processorContextElement.processor
	if proc == nil {
		return true, errors.New("chain element initialization was invalid: No processor specified")
	}

	// log.Printf("Processor(%s) received status request", el.processor.conn.RemoteAddr().String())

	// Create answer message
	var outMessage JSONStatusMessage

	if proc.solving {
		outMessage.Status = "solving"
		var statusesArray = make([]JSONSolvingStatus, len(proc.statusLayers))
		for i, val := range proc.statusLayers {
			var stat JSONSolvingStatus
			if val == nil {
				stat = JSONSolvingStatus{
					Run:        0,
					Iteration:  0,
					BestLength: 0,
				}
			} else {
				stat = JSONSolvingStatus{
					Run:        val.runNumber,
					Iteration:  val.iteration,
					BestLength: val.minMetric,
				}
			}
			statusesArray[i] = stat
		}
		var err error
		outMessage.Data, err = json.Marshal(statusesArray)
		if err != nil {
			log.Printf("Failed to create status message: %v", err)
		}
	} else {
		if proc.answerReady {
			outMessage.Status = "ready"
			var schedule JSONSchedule
			var taskIDs = proc.answer.Schedule().TaskIDs()
			schedule.ScheduleCount = uint64(len(taskIDs))
			schedule.ScheduleRecords = make([]JSONScheduleRecord, 0, schedule.ScheduleCount)
			for _, tID := range taskIDs {
				var record JSONScheduleRecord
				record.ID = uint64(tID)
				record.Name = proc.problem.Graph().Task(tID).Name()
				time, ok := proc.answer.Schedule().TaskStartTime(tID)
				if ok {
					record.StartTime = int64(time)
				}
				record.EndTime = int64(time) + int64(proc.problem.Graph().Task(tID).Effort())
				schedule.ScheduleRecords = append(schedule.ScheduleRecords, record)
			}
			var err error
			outMessage.Data, err = json.Marshal(schedule)
			if err != nil {
				log.Printf("Failed to create status message: %v", err)
			}
		} else if proc.failed {
			outMessage.Status = "error"
			var err error
			outMessage.Data, err = json.Marshal(proc.failMsg)
			if err != nil {
				log.Printf("Failed to create status message: %v", err)
			}
		} else {
			outMessage.Status = "notstarted"
		}
	}

	// Send answer to client

	var writer = bufio.NewWriter(proc.conn)
	bytes, err := json.Marshal(outMessage)
	if err != nil {
		log.Printf("Failed to create status message: %v", err)
	}

	// log.Printf("Processor(%s) is sending status: \n%s", el.processor.conn.RemoteAddr().String(), bytes)

	writer.Write(bytes)
	writer.WriteRune('\000')
	writer.Flush()

	return captured, nil
}

type startSolvingChainElement struct {
	processorContextElement
}

func newStartSolvingChainElement() messageProcessChainElement {
	return &startSolvingChainElement{}
}

func (el *startSolvingChainElement) Process(msg JSONMessage) (bool, error) {
	var captured = false
	if msg.Type != "start" {
		return captured, nil
	}

	captured = true
	log.Printf("Processor(%s) received start request", el.processor.conn.RemoteAddr().String())

	var proc = el.processorContextElement.processor
	if proc == nil {
		return true, errors.New("chain element initialization was invalid: No processor specified")
	}

	if proc.problem == nil {
		return true, errors.New("problem have not got before start solving")
	}

	if proc.config.AntCount == 0 {
		return true, errors.New("count of ants must be > 0")
	}

	if proc.config.IterationCount == 0 {
		return true, errors.New("count of iterations must be > 0")
	}

	if proc.config.RunCount == 0 {
		return true, errors.New("count of runs must be > 0")
	}

	// Now we lock problem, config requests
	proc.solving = true
	go func() {
		var solversChan = el.prepareSolve()
		var answersChan = el.startSolve(solversChan)

		var bestAns dekaants.Answer
		var bestLength schedule.Time
		var intAnswerNumber = 0
		for ans := range answersChan {

			if ans.Error() != nil {
				proc.failed = true
				proc.failMsg = fmt.Sprint(ans.Error())
				log.Printf("Processor(%s) answer #%d failed: %s", el.processor.conn.RemoteAddr().String(),
					intAnswerNumber, proc.failMsg)
				continue
			}

			log.Printf(
				"Processor(%s) answer #%d ready. Length is %f",
				el.processor.conn.RemoteAddr().String(),
				intAnswerNumber,
				ans.Metric(),
			)

			if bestAns == nil {
				bestAns = ans
				bestLength = schedule.Time(bestAns.Metric())
			} else if schedule.Time(ans.Metric()) < bestLength {
				bestAns = ans
				bestLength = schedule.Time(bestAns.Metric())
			}

			intAnswerNumber++
		}

		if !proc.failed {
			proc.answer = bestAns.(scheduleantsolver.ScheduleAnswer)
			proc.answerReady = true

			log.Printf(
				"Processor(%s) best answer ready. Length is %f",
				el.processor.conn.RemoteAddr().String(),
				bestAns.Metric(),
			)
		} else {
			log.Printf(
				"Processor(%s) failed",
				el.processor.conn.RemoteAddr().String(),
			)
		}

		proc.solving = false
		proc.solvingCond.Broadcast()

	}()

	return captured, nil
}

func (el *startSolvingChainElement) prepareSolve() <-chan dekaants.Solver {
	var proc = el.processor

	var outChannel = make(chan dekaants.Solver)

	if 0 < len(el.processor.solvers) {
		for _, s := range el.processor.solvers {
			s.Stop()
		}
	}
	el.processor.solvers = make([]dekaants.Solver, 0)

	go func() {
		var runNumber uint32
		proc.statusLayers = make([]*StatusLayer, proc.config.RunCount)
		for runNumber = 0; runNumber < proc.config.RunCount; runNumber++ {
			log.Printf("Processor(%s) Preparing run #%d", el.processor.conn.RemoteAddr().String(), runNumber)

			var antFactory = scheduleantsolver.NewScheduleAntFactory(proc.config.ChooseConfig, proc.config.Config)
			proc.statusLayers[runNumber] = newStatusLayer(uint64(runNumber)).(*StatusLayer)
			var solver = dekaants.NewConstIterationSolver(uint64(proc.config.IterationCount), uint(proc.config.AntCount), antFactory, proc.statusLayers[runNumber])
			outChannel <- solver
			el.processor.solvers = append(el.processor.solvers, solver)
		}
		close(outChannel)
	}()

	return outChannel
}

func (el *startSolvingChainElement) startSolve(inputSolvers <-chan dekaants.Solver) <-chan dekaants.Answer {
	var outChannel = make(chan dekaants.Answer)

	go func() {
		var wg sync.WaitGroup
		for solver := range inputSolvers {
			wg.Add(1)
			go func(slv dekaants.Solver) {
				var antEnvironment = scheduleantsolver.NewScheduleEnvironment(el.processor.problem)
				defer wg.Done()
				outChannel <- slv.Solve(antEnvironment)
			}(solver)
		}
		wg.Wait()
		close(outChannel)
	}()

	return outChannel
}
