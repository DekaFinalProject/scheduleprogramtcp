package main

import "gitlab.com/DekaFinalProject/scheduleantsolver"

type SolverOptions struct {
	IterationCount uint32
	AntCount       uint32
	RunCount       uint32

	Config       scheduleantsolver.MaxMinStrategyConfig
	ChooseConfig scheduleantsolver.ProportionalChooseStrategyConfig
}
