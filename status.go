package main

import "encoding/json"

type JSONStatusMessage struct {
	Status string
	Data   json.RawMessage
}

type JSONSolvingStatus struct {
	Run        uint64
	Iteration  uint64
	BestLength float64
}

type JSONScheduleRecord struct {
	ID        uint64
	Name      string
	StartTime int64
	EndTime   int64
}

type JSONSchedule struct {
	ScheduleCount   uint64
	ScheduleRecords []JSONScheduleRecord
}
