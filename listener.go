package main

import (
	"log"
	"net"
	"sync"
)

type Listener struct {
	running bool

	listener net.Listener

	wg *sync.WaitGroup

	wgProcessors sync.WaitGroup
}

func NewListener(address string, wg *sync.WaitGroup) *Listener {
	var out Listener

	out.running = false
	out.wg = wg

	var err error = nil
	out.listener, err = net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("Failed to open TCP server: %v", err)
	}

	return &out
}

func (l *Listener) Listen() {
	defer l.wg.Done()

	l.running = true

	log.Printf("Started listening on %s", l.listener.Addr().String())

	for l.running {
		conn, err := l.listener.Accept()
		if err != nil {
			if l.running {
				log.Printf("Accept failed: %v", err)
			} else {
				return
			}

		}

		log.Printf("Accepted new connection from %s", conn.RemoteAddr().String())
		l.wgProcessors.Add(1)
		var processor = newConnectionProcessor(conn)
		go processor.Process(&l.wgProcessors)
	}

	l.wgProcessors.Wait()
}

func (l *Listener) Stop() {
	l.running = false
	l.listener.Close()
}

func (l *Listener) WaitForStop() {
	l.wg.Wait()
}
