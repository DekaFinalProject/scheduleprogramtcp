package main

import (
	"encoding/json"
	"log"

	"gitlab.com/DekaFinalProject/schedulelib/problemloader"
	"gitlab.com/DekaFinalProject/schedulelib/schedule"
)

// JSON types
type JSONResourceRequrement struct {
	ResourceID uint64
	Amount     float64
}

type JSONTask struct {
	ID     uint64
	Name   string
	Effort uint64

	PreviousTasksCount uint64
	PreviousTasks      json.RawMessage

	ResourceRequrementsCount uint64
	ResourceRequrements      json.RawMessage
}

type JSONTimePoint struct {
	Time   int64
	Amount float64
}

type JSONResource struct {
	ID         uint64
	Name       string
	Consumable bool

	TimePointsCount uint64
	TimePoints      json.RawMessage
}

type JSONProblem struct {
	TasksCount uint64
	Tasks      json.RawMessage

	ResourcesCount uint64
	Resources      json.RawMessage
}

// JSON based loading
type JSONProblemLoader struct {
	problemloader.Loader

	factory   schedule.Factory
	converter *problemloader.Converter

	data json.RawMessage
}

func NewJSONProblemLoader(data json.RawMessage, factory schedule.Factory) problemloader.Loader {
	var out JSONProblemLoader

	out.factory = factory
	out.data = data
	out.converter = problemloader.NewConverter()

	return &out
}

func (l *JSONProblemLoader) Load() (schedule.Problem, error) {
	// Process
	var jsonProblem JSONProblem

	if err := json.Unmarshal([]byte(l.data), &jsonProblem); err != nil {
		log.Printf("Failed to read from tcp connection: %v", err)
	}

	if err := l.loadTasks(jsonProblem.Tasks, jsonProblem.TasksCount); err != nil {
		log.Printf("Failed to read tasks: %v", err)
	}
	if err := l.loadResources(jsonProblem.Resources, jsonProblem.ResourcesCount); err != nil {
		log.Printf("Failed to read resources: %v", err)
	}

	problem, err := l.converter.Problem()
	if err != nil {
		return nil, err
	}

	return problem, nil
}

func (l *JSONProblemLoader) loadTasks(raw json.RawMessage, count uint64) error {
	// Load raw list of tasks
	var tasks = make([]JSONTask, count)
	err := json.Unmarshal(raw, &tasks)
	if err != nil {
		return err
	}

	for _, value := range tasks {
		// Get values
		var taskToAdd problemloader.Task = problemloader.NewTask(schedule.Identifier(value.ID))
		taskToAdd.SetName(value.Name)
		taskToAdd.SetEffort(schedule.Time(value.Effort))

		// Get previous tasks
		prevs, err := l.getPreviousTasks(value)
		if err != nil {
			return err
		}

		for _, prevID := range prevs {
			taskToAdd.AddPreviousTask(schedule.Identifier(prevID))
		}

		// Get requirements
		reqs, err := l.getRequrements(value)
		if err != nil {
			return err
		}

		for _, reqVal := range reqs {
			taskToAdd.SetResourceRequirement(
				schedule.Identifier(reqVal.ResourceID),
				schedule.ResourceAmount(reqVal.Amount),
			)
		}

		// Add task
		l.converter.AddTask(taskToAdd)
	}

	return nil
}

func (l *JSONProblemLoader) getPreviousTasks(t JSONTask) ([]uint64, error) {
	var out = make([]uint64, t.PreviousTasksCount)
	if err := json.Unmarshal(t.PreviousTasks, &out); err != nil {
		return out, err
	}

	return out, nil
}

func (l *JSONProblemLoader) getRequrements(t JSONTask) ([]JSONResourceRequrement, error) {
	var out = make([]JSONResourceRequrement, t.ResourceRequrementsCount)
	if err := json.Unmarshal(t.ResourceRequrements, &out); err != nil {
		return out, err
	}

	return out, nil
}

func (l *JSONProblemLoader) loadResources(raw json.RawMessage, count uint64) error {
	// Load raw list of resources
	var resources = make([]JSONResource, count)
	if err := json.Unmarshal(raw, &resources); err != nil {
		return err
	}

	for _, jsonres := range resources {
		res := l.factory.NewResource(schedule.Identifier(jsonres.ID))
		res.SetName(jsonres.Name)
		res.SetConsumable(jsonres.Consumable)

		timepoints, err := l.getTimePoints(jsonres)
		if err != nil {
			return err
		}

		var previousAmount schedule.ResourceAmount = 0
		for _, tp := range timepoints {

			var delta = schedule.ResourceAmount(tp.Amount) - previousAmount
			// A zero result will cause no update
			if 0 < delta {
				res.Add(delta, schedule.Time(tp.Time))
			} else if delta < 0 {
				res.Subtract(-delta, schedule.Time(tp.Time))
			}
			previousAmount = schedule.ResourceAmount(tp.Amount)
		}

		l.converter.AddResource(res)
	}

	return nil
}

func (l *JSONProblemLoader) getTimePoints(res JSONResource) ([]JSONTimePoint, error) {
	var out = make([]JSONTimePoint, res.TimePointsCount)
	if err := json.Unmarshal(res.TimePoints, &out); err != nil {
		return out, err
	}

	return out, nil
}
